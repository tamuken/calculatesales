package chapter7.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter7.beans.User;
import chapter7.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();
        if (isValid(request, messages) == true) {

            User user = new User();
            user.setName(request.getParameter("name"));
            user.setLoginid(request.getParameter("loginid"));
            user.setPassword(request.getParameter("password"));
            user.setBranch(Integer.parseInt((request.getParameter("branch"))));
            user.setDepartment(Integer.parseInt((request.getParameter("department"))));

            new UserService().register(user);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("signup");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String name = request.getParameter("name");
        String password = request.getParameter("password");
        String conPassword = request.getParameter("conPassword");
        String loginid = request.getParameter("loginid");

        if (StringUtils.isEmpty(name) == true) {
            messages.add("アカウント名を入力してください");
        }
        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        }//a.equals(b)　aとbの文字列の比較
        if (!(password.equals(conPassword))) {
            messages.add("パスワードが一致していません");
        }
        if(name.length()>10) {
        	messages.add("10文字以下で入力してください");
        }
        if ((!loginid.matches("^.{6,20}$")) || (!loginid.matches("^[0-9a-zA-Z]+$"))) {
        	messages.add("ログインIDは半角文字6文字以上20文字以下で入力してください");
        }
        if ((!password.matches("^.{6,20}$")) || (!password.matches("^[0-9a-zA-Z]+$"))) {
        	messages.add("パスワードは半角文字6文字以上20文字以下で入力してください");
        }



        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}
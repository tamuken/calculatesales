package chapter7.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter7.beans.User;
import chapter7.service.UserService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        User user = (User) request.getSession().getAttribute("loginUser");
        boolean isShowMessageForm;
        if (user != null) {
            isShowMessageForm = true;
        } else {
            isShowMessageForm = false;
        }

     //   List<User> messages = new MessageService().getMessage();

    //    request.setAttribute("messages", messages);
    //    request.setAttribute("isShowMessageForm", isShowMessageForm);
        HttpSession session = request.getSession();
        //セッションよりログインユーザーの情報を取得
        User loginUser = (User) session.getAttribute("loginUser");
        //ログインユーザー情報のidを元にDBからユーザー情報取得
        List<User> editUser = new UserService().getUser();
        request.setAttribute("editUser", editUser);

        request.getRequestDispatcher("/top.jsp").forward(request, response);
    }
}

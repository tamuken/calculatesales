package chapter7.dao;

import static chapter7.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter7.beans.User;
import chapter7.exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append(" name");
            sql.append(", loginid");
            sql.append(", password");
            sql.append(", branch");
            sql.append(", department");
            sql.append(") VALUES (");
            sql.append(" ?"); // name
            sql.append(", ?"); // loginid
            sql.append(", ?"); // password
            sql.append(", ?"); //  branch
            sql.append(", ?"); // department
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getName());
            ps.setString(2, user.getLoginid());
            ps.setString(3, user.getPassword());
            ps.setInt(4, user.getBranch());
            ps.setInt(5, user.getDepartment());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }


    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String loginid = rs.getString("loginid");
                String password = rs.getString("password");
                String conpassword = rs.getString("conpassword");
                String branch = rs.getString("branch");
                String department = rs.getString("department");

                User user = new User();
                user.setId(id);
                user.setLoginid(loginid);
                user.setName(name);
                user.setPassword(password);
                user.setConPassword(conpassword);
                user.setBranch((Integer.parseInt((branch))));
                user.setDepartment((Integer.parseInt((department))));


                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}
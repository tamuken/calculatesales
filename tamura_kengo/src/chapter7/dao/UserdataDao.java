package chapter7.dao;

import static chapter7.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter7.beans.User;
import chapter7.exception.SQLRuntimeException;

public class UserdataDao {

    public List<User> getUser(Connection connection) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("select ");
            sql.append("users.id,");
            sql.append("users.loginid, ");
            sql.append("users.name, ");
            sql.append("branchs.branch_name, ");
            sql.append("departments.department_name, ");
            sql.append("users.stop ");
            sql.append("from users ");
            sql.append("inner join branchs  ");
            sql.append("on users.branch=branchs.id ");
            sql.append("join departments");
            sql.append("on users.department=departments.id");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<User> ret = toUserDataList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserDataList(ResultSet rs)
            throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
            	String id = rs.getString("id");;
                String name = rs.getString("name");
                int loginid = rs.getInt("loginid");
                int branch = rs.getInt("branch");
                int department = rs.getInt("department");

                User useList = new User();
                useList.setId(Integer.parseInt(id));
                useList.setName(name);
                useList.setLoginid(String.valueOf(loginid));
                useList.setBranch(branch);
                useList.setDepartment(department);

                ret.add(useList);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}

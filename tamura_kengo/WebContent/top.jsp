<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>tamura_kengo</title>
    </head>
    <body>
    <div class="header">
    <c:if test="${ empty loginUser }">
        <a href="signup">登録する</a>
        <a href="login">編集する</a>
    </c:if>
    <c:if test="${ not empty loginUser }">
        <a href="./">ホーム</a>
        <a href="settings">設定</a>
        <a href="logout">ログアウト</a>
    </c:if>
</div>
            <div class="copyright"> Copyright(c)YourName</div>
        </div>
    </body>
<div class="user">
	//処理の繰り返し
    <c:forEach items="${user}" var="user">
            <div class="user">
                <div class="account-name">
                    <span class="loginid"><c:out value="${message.loginid}" /></span>
                    <span class="name"><c:out value="${message.name}" /></span>
                </div>
                <div class="branch"><c:out value="${message.branch}" /></div>
                <div class="department"><fmt:formatDate value="${message.department}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
            </div>
    </c:forEach>
</div>

<body>

 <table border="1">
    <tr>
      <th>ID</th>
      <th>名前</th>
      <th>支店</th>
      <th>部署・役職</th>
    </tr>
    <tr>
      <td>田中</td>
      <td>27</td>
    </tr>

  </table>
</body>

</html>
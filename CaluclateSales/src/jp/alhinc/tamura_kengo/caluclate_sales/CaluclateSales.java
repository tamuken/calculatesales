package jp.alhinc.tamura_kengo.caluclate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CaluclateSales {

	public static void main(String[] args) {
		Map<String, String> map = new HashMap<>();
		{
			//売上げマップ
			Map<String, Long> testmap = new HashMap<>();
			BufferedReader br = null;
			try {
				File branch = new File(args[0], "branch.lst");

				//定義したうえで実行する前にエラー検索
				if (!branch.exists()) {
					System.out.println("支店定義ファイルが存在しません");
					return;
				}
				//支店定義ファイルを開く
				FileReader fr = new FileReader(branch);
				br = new BufferedReader(fr);
				String line;
				//支店定義ファイルを一行ずつ読込
				while ((line = br.readLine()) != null) {
					String[] str = line.split(",");
					//支店コード、支店名を保持。
					if (!str[0].matches("^\\d{3}") || (str.length != 2)) {
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}
					map.put(str[0], str[1]);
					//aかb→(||)。要素数2
					File dir = new File(args[0]);
					//支店コードと売上初期値をput
					testmap.put(str[0], 0L);
				}
				//フィルタを作成
				FilenameFilter filter = new FilenameFilter() {
					//ファイルを検索
					public boolean accept(File file, String str) {
						String rcd = "^\\d{8}.rcd$";
						//fileの場所にある、strのファイル名
						File saleslist = new File(file, str);
						if (str.matches(rcd) && saleslist.isFile()) {
							return true;
						} else {
							return false;//該当する内容を検索
						}
					}
				};
				File files = new File(args[0]);
				File[] rcdFiles = files.listFiles(filter);

				//ファイルの要素数を取得して変数にする
				int count = rcdFiles.length - 1;
				//ファイル名を取得して変数にする
				String small = rcdFiles[0].getName();
				//スプリットで分ける。「.」の使い方注意
				String[] min = small.split("\\.");
				//文字列を数値に変える
				int codemin = Integer.parseInt(min[0]);
				String large = rcdFiles[count].getName();
				String[] max = large.split("\\.");
				int codemax = Integer.parseInt(max[0]);
				//カウント(要素数)=Max-Min
				if (!(count == codemax - codemin)) {
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}

				//ファイルの場所を定義
				File branchout = new File(args[0], "branch.out");
				FileWriter fw = new FileWriter(branchout);

				for (File file : rcdFiles) {
					br = null;
					br = new BufferedReader(new FileReader(file));
					//売上げファイル1行目の読み取り
					String code = br.readLine();
					//売上げファイル2行目
					long uriage = Long.parseLong(br.readLine());
					//売上げファイル3行目
					String salesline = br.readLine();
					if (!testmap.containsKey(code)) {
						System.out.println(file.getName() + "の支店コードが不正です");
						return;
					}
					testmap.put(code, testmap.get(code) + uriage);
					//longからStringへ変換。length()で長さ。
					if (String.valueOf(testmap.get(code) + uriage).length() > 10) {
						System.out.println("合計金額が10桁を超えました");
						return;
					}
					if (salesline != null) {
						System.out.println(file.getName() + "のフォーマットが不正です");
						return;
					}
				}
				//支店別集計ファイルの作成
				BufferedWriter pw = new BufferedWriter(new BufferedWriter(fw));
				//１つ目のマップから情報を取得する
				for (Map.Entry<String, String> entry : map.entrySet()) {
					//取得した内容をもとに支店コード等取得し、ファイルに書き込む。
					pw.write(entry.getKey() + "," + entry.getValue() + "," + testmap.get(entry.getKey()));
					//改行
					pw.newLine();
				}
				pw.close();
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("予期せぬエラーが発生しました");
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
					}
				}
			}
		}
	}
}
